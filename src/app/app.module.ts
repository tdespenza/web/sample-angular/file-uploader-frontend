import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from "@angular/router";

import { UPLOAD_DIRECTIVES } from 'ng2-uploader/ng2-uploader';
import { routingConfig } from './app.routes.ts';
import { AppComponent } from './app.component';
import { ListComponent } from './component/list/list.component';
import { HomeComponent } from './component/home/home.component';

@NgModule({
  declarations: [
    UPLOAD_DIRECTIVES,
    AppComponent,
    ListComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routingConfig
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
