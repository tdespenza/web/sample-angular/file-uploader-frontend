/**
 * Created by tdespenza on 11/14/16.
 */

import { Routes, RouterModule } from "@angular/router";

import { HomeComponent } from './component/home/home.component';
import { ListComponent } from './component/list/list.component';

const APP_ROUTES: Routes = [
  { path: '', component: HomeComponent },
  { path: 'list', component: ListComponent }
]

export const routingConfig = RouterModule.forRoot(APP_ROUTES);
