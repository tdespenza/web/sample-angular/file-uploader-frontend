import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { ListService } from "../../service/list/list.service";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  providers: [ListService, Http]
})
export class ListComponent implements OnInit {
  private metadata:any;

  constructor(private listService:ListService) {
  }

  ngOnInit() {
    this.listService.getMetadata()
      .subscribe(
        (data:any) => this.metadata = data
      );
  }

  isEmpty() {
    return !(
      this.metadata
      && typeof this.metadata !== 'undefined'
      && this.metadata.length > 0
    );
  }

  convertToDateObject(date) {
    return new Date(
      date[0],      //year
      date[1] - 1,  //month
      date[2],      //day
      date[3],      //hour
      date[4],      //minute
      date[5]       //second
    );
  }
}
