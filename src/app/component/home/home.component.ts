import { Component, OnInit, NgZone, trigger, state, style, transition, animate } from '@angular/core';

const URL = 'http://localhost:8080/api/upload';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  animations: [
    trigger('uploadComplete', [
      state('normal', style({
        'display': 'block',
        'opacity': '0'
      })),
      state('complete', style({
        'opacity': '1'
      })),
      transition('* <=> *', animate('.5s'))
    ])
  ]
})
export class HomeComponent implements OnInit {
  private state:string;
  private zone:NgZone;
  private response:any;
  private hasBaseDropZoneOver:boolean;
  private options:Object;

  constructor() {
  }

  ngOnInit() {
    this.state = 'normal';
    this.zone = new NgZone({ enableLongStackTrace: false });
    this.hasBaseDropZoneOver = false;
    this.options = {
      url: URL,
      customHeaders: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Headers': 'X-Requested-With'
      }
    };
  }

  handleUpload(data:any):void {
    this.zone.run(() => {
      this.response = data;
      this.hasBaseDropZoneOver = false;
      this.state = 'complete';
      var self = this;
      setTimeout(() => self.state = 'normal', 3000)
    });
  }

  fileOverBase(e:any):void {
    this.hasBaseDropZoneOver = e;
  }
}
