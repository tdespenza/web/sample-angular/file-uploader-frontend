import { browser, element, by } from 'protractor';

export class FrontendPage {
  navigateTo() {
    return browser.get('/');
  }

  getNavbarBrand() {
    return element(by.css('app-root navbar-brand')).getText();
  }

  getHomeButton() {
    return element(by.css('app-root navbar-nav li:first > a')).getText();
  }

  getListButton() {
    return element(by.css('app-root navbar-nav li:last > a')).getText();
  }

  getDropZoneArea() {
    return element(by.css('app-root container .drop-zone-padding label')).getText();
  }
}
